package net.devrob.tutorialkotlindevrob

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import net.devrob.tutorialkotlindevrob.vista.DatosContactoActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startActivity(Intent(this, DatosContactoActivity::class.java))
    }
}
