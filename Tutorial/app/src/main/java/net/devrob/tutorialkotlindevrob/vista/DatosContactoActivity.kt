package net.devrob.tutorialkotlindevrob.vista

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_datos_contacto.*
import net.devrob.tutorialkotlindevrob.R

class DatosContactoActivity:AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_datos_contacto)

        toolbar!!.title = "Detalles del contacto"
        setSupportActionBar(toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_datos_contacto, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.guardar -> { mostrarInformacion() }
        }
        return true
    }

    fun mostrarInformacion(){
        var nombreContacto = txtContacto.text.toString()
        var telefono = txtTelefono.text.toString()
        var correo = txtCorreo.text.toString()

        var alert = AlertDialog.Builder(this)
        alert.setTitle("Valores ingresados")
        alert.setMessage("Nombre: " + nombreContacto + " Teléfono: " + telefono + " Correo: " + correo)
        alert.setPositiveButton("Aceptar", {dialog, i -> dialog.dismiss() })
        alert.show()
    }

}